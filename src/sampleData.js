/**
 *  Sample Data for a student Grades Table
 *  ---
 */

export const SampleData = [
  {
    examMaxPercentage: 20,
    examsTotalGrade: null,
    examsTotalGradePercentage: null,
    examsGradingAndSubmission: [
      {
        name: "Midterm 1",
        status: "Not Submitted",
        isLate: null,
        subfolderName: "midterms",
        subFolderMaxPercentage: 25,
        // Grading section
        assignmentGrade: null,
        assignmentMaxGrade: 20,
        due: 'May 16, 11:59 PM'
      },
    ],
    homeworkMaxPercentage: 20,
    homeworkTotalGrade: 15,
    homeworkTotalGradePercentage: 15,
    homeworkGradingAndSubmission: [
      {
        name: "Homework 1",
        status: "Graded",
        isLate: null,
        subfolderName: null,
        subfolderMaxPercentage: null,
        // Grading section
        assignmentGrade: 15,
        assignmentMaxGrade: 20,
        due: 'May 16, 11:59 PM'
      },
      {
        name: "Homework 2 - Taylor Series and Maclaurin Series",
        status: "Not Submitted",
        isLate: null,
        subfolderName: null,
        subfolderMaxPercentage: null,
        // Grading section
        assignmentGrade: null,
        assignmentMaxGrade: 20,
        due: 'May 23, 11:59 PM'
      },
    ],
    quizzesMaxPercentage: 20,
    quizzesTotalGrade: 15,
    quizzesTotalGradePercentage: 15,
    quizzesGradingAndSubmission: [
      {
        name: "Quiz 1",
        status: "Submitted",
        isLate: true,
        subfolderName: null,
        subFolderMaxPercentage: null,
        // Grading section
        assignmentGrade: 15,
        assignmentMaxGrade: 20,
        due: 'May 16, 11:59 PM'
      },
    ],
  },
];
